import 'package:flutter/material.dart';
import 'package:gg_progr_toniko_admin/constants.dart';
import 'package:gg_progr_toniko_admin/screens/home.dart';
import 'package:gg_progr_toniko_admin/screens/landing.dart';
import 'package:gg_progr_toniko_admin/screens/login.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appName,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      //home: const MyHomePage(title: title + " (" + appVersion + ")"),
      routes: {
        '/': (context) => Landing(),
        '/login': (context) => Login(),
        '/home': (context) => MyHomePage(title: 'Login Demo'),
      },
    );
  }
}
