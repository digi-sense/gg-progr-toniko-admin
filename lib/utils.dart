import 'package:flutter/material.dart';

void showMessage(String text, BuildContext? context) {
  ScaffoldMessenger.of(context!).showSnackBar(SnackBar(content: Text(text)));
}
